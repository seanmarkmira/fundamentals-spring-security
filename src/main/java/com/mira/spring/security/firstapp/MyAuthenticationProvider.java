package com.mira.spring.security.firstapp;

import java.util.Arrays;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

//This is the AuthenticationProvider part of the flow
/*
 * This method: UsernamePasswordAuthenticationToken(), will be the one being returned from Authentication Provider -> Authentication Manager ->
 * Authentication Filter to WHERE it will either store it to Security Context (if it is pass) or will throw an error through the AuthenticationFailureHandler
 * 
 * This method: supports(), is scanned by the Authentication Manager to check if a certain authentication method is supported. In this example, it says
 * that UsernamePasswordAuthenticationToken.class is being supported.  
 */

/*
 * NOTE: This class will be checked by the Authentication Manager if it supports a specific authentication mechanism that is being pass to Spring Boot
 */
@Component
public class MyAuthenticationProvider implements AuthenticationProvider{
	//Authentication Logic
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String userName = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		if("user".equals(userName) && "password".equals(password)) {
			return new UsernamePasswordAuthenticationToken(userName, password, Arrays.asList());
		}else {
			throw new BadCredentialsException("Invalid Username or Password");
		}
	}
	//This class supports what authentication 
	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}

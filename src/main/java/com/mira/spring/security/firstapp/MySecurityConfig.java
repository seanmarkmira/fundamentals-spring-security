package com.mira.spring.security.firstapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

//This is the User Details Service and Password Encoder in the flow
/*
 * 	There are 2 methods that are being overridden in the old code. (1) Is the configure that accepts AuthenticationManager  
 * 	and (2) the other is the configure that accepts the HttpSecurity.
 * 		(1) We are customizing the user service by utilizing inMemory and adding user details for the user login
 * 		(2) We are telling spring that use httpBasic() for authentication and then all request must be authenticated
 */
@Configuration
public class MySecurityConfig{
	@Autowired
	private MyAuthenticationProvider authenticationProvider;
	
	@Bean
	public AuthenticationManager authManager(HttpSecurity http, PasswordEncoder passwordEncoder, UserDetailsService userDetailService) throws Exception{
		return http.getSharedObject(AuthenticationManagerBuilder.class).authenticationProvider(authenticationProvider).build();
	}
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
		//Different of authentication forms, example: httpBasic() and formLogin()
		http.httpBasic();
		//http.formLogin();
		
		//for /hello endpoint we need to be authenticated by in any request other than that, denyAll()
		//http.authorizeRequests().requestMatchers("/hello").authenticated().anyRequest().denyAll();
		
		http.authorizeRequests().requestMatchers("/hello").authenticated();
		//This is adding filters. First argument is our Filter and then next is the class from the Spring Security to which we want to chain our filter.
		http.addFilterBefore(new MySecurityFilter(), BasicAuthenticationFilter.class);
		return http.build();
	}
	
	@Bean
	public InMemoryUserDetailsManager userDetailsService(PasswordEncoder passwordEncoder) {
	    UserDetails user = User.withUsername("user")
	        .password(passwordEncoder.encode("password"))
	        .roles("USER")
	        .build();

//	    UserDetails admin = User.withUsername("admin")
//	        .password(passwordEncoder.encode("admin"))
//	        .roles("USER", "ADMIN")
//	        .build();

//	    return new InMemoryUserDetailsManager(user, admin);
	    return new InMemoryUserDetailsManager(user);
	}
	
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}



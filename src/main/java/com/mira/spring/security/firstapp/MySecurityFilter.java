package com.mira.spring.security.firstapp;

import java.io.IOException;

import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

//This is the Authentication Filter of the Spring Security flow
/*
 * 3 things to remember:
 * 1. code above and below chain.doFilter(request, response); will be executed based on their positioning. See sysout
 * 2. For us to utilize this, you must add this to our authenticate Authentication Manager and add the code below 
 * http.addFilterBefore(new MySecurityFilter(), BasicAuthenticationFilter.class);
 */

//There are other interfaces that you can apply for your filter. For example,
/*
 * 1. public class MySecurityFilter implements GenericFilterBean{}: If you have requirements that needed initialization of properties from the 
 * Web.xml then you may look in this interface.
 * 2. public class MySecurityFilter implements OncePerRequestFilter{}: If you want to initiate the filter once per request only
 */
public class MySecurityFilter implements Filter{

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("Before");
		chain.doFilter(request, response);
		System.out.println("After");
	}

}

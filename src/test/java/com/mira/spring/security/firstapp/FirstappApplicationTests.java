package com.mira.spring.security.firstapp;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

@SpringBootTest
class FirstappApplicationTests {
	@Test
	void testPasswordEncoders() {
		//Please look into the new implementation of pbkdf2PasswordEncoder as this is deprecated, 
		//there is a new approach to implement Pbkdf2PasswordEncoder and SCryptPasswordEncoder
		//Pbkdf2PasswordEncoder pbkdf2PasswordEncoder = new Pbkdf2PasswordEncoder("salt", 100000, 128, 0);
		
		System.out.println("test here");
		System.out.println(new BCryptPasswordEncoder().encode("password"));
		//System.out.println(new pbkdf2PasswordEncoder().encode("password"));
		//See documentation for SCryptPasswordEncoder (cpu cost, memory cost, parallelization, key length, and salt length)
		System.out.println(new SCryptPasswordEncoder(2, 2, 2, 2, 2).encode("seanmira"));
		System.out.println(new BCryptPasswordEncoder().encode("seanmira"));
		
		Map<String, PasswordEncoder> encoders = new HashMap<>();
		encoders.put("bcrypt", new BCryptPasswordEncoder());
		//encoders.put("scrypt", new SCryptPasswordEncoder());
		//This is to be able to dynamically choose an encoding. Through DelegatingPasswordEncoder we can set a map of different encoder
		//and dynamically choose what encoding we want
		System.out.println(new DelegatingPasswordEncoder("bcrypt", encoders).encode("password"));
		System.out.println("test here");
	}

}

